from operator import itemgetter
from utils.csv_utils import get_csv_data
from utils.data_types import get_header_with_types, get_data_converted, get_data_in_tuples
from utils.cols_generator import get_mysql_cols
from controller.db_mysql import generate_table, insert_values

def csv_insert_mysql(csv_path:str, table_name:str, database:str):
  csv_data = get_csv_data(csv_path)

  header, data, count = itemgetter('header', 'data', 'count')(csv_data)

  print(f"Se generará la tabla {database}.{table_name}, con {len(header)} atributos y {count} registros")
  print()
  input("Presiona cualquier tecla para proceder...")
  print()

  header_with_types = get_header_with_types(header, data[0])

  mysql_cols = get_mysql_cols(header_with_types)

  cols_len = len(mysql_cols)

  converted_data = get_data_converted(data, header_with_types, True)

  # for row in converted_data:
  #   row_len = len(row.items())
  #   if row_len != cols_len:
  #     print(f"{row_len} : {cols_len}")

  data_in_tuples = get_data_in_tuples(converted_data)

  try:
    print(f"Generando {database}.{table_name}...")
    generate_table(database, table_name, mysql_cols)
    try:
      print(f"Insertando {count} registros en {database}.{table_name}...")
      insert_values(database, table_name, header, data_in_tuples)
      print(f'Se han insertado {count} registros')
    except:
      print('Ha ocurrido un error insertando en la base de datos')
  except:
    print('Ha ocurrido un error generando la base de datos')
    
  