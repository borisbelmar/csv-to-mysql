# Python CSV to MySQL

**Versión**: 1.0

Pequeño programa CLI para insertar ficheros CSV en MySQL.

## Uso:
Para iniciar el programa, debemos montar el entorno.

```
# Iniciar el servicio de MySQL
docker-compose up -d

# Sincronizar dependencias con pipenv 
pipenv sync

# Ejecutar el CLI
pipenv run python3 . </path/to/file.csv>
```

## TODO:
- [ ] Generar ejecutable standalone
- [ ] Mejorar manejo de excepciones
- [ ] Cargar configuración desde JSON, YAML o TOML
- [ ] Confirmar estructura de la tabla
- [ ] Agregar test unitarios