from mysql.connector import connect, Error
from utils.cols_generator import get_string_cols, get_string_insert_cols # pylint: disable=import-error

DB_CONFIG = {
  'host': 'localhost',
  'user': 'root',
  'password': 'root',
  'auth_plugin': 'mysql_native_password',
  'port': 9999
}

def get_connection(database:str):
  return connect(
    host = DB_CONFIG['host'],
    user = DB_CONFIG['user'],
    password = DB_CONFIG['password'],
    database = database,
    auth_plugin = DB_CONFIG['auth_plugin'],
    port = DB_CONFIG['port']
  )

def generate_table(database:str, table_name:str, cols:list):
  connection = connect(
    host = DB_CONFIG['host'],
    user = DB_CONFIG['user'],
    password = DB_CONFIG['password'],
    auth_plugin = DB_CONFIG['auth_plugin'],
    port = DB_CONFIG['port']
  )
  cursor = connection.cursor()
  cursor.execute(f'CREATE DATABASE IF NOT EXISTS {database}')
  cursor.execute(f'USE {database}')
  try:
    cursor.execute(f'CREATE TABLE {table_name} ({get_string_cols(cols)})')
  except Error as err:
    if err.errno == 1050:
      print(f"La tabla {database}.{table_name} ya existe")
      override = input(f"Desea sobreescribir {database}.{table_name}? (y/N)")
      print("")
      if override.lower() == "y":
        print(f"Sobreescribiendo la tabla {database}.{table_name}...")
        cursor.execute(f"DROP TABLE IF EXISTS {table_name}")
        cursor.execute(f'CREATE TABLE {table_name} ({get_string_cols(cols)})')
      else:
        print(f"No se puede generar la tabla debido a que ya existe")
        exit()
    else:
      raise
  finally:
    cursor.close()
    connection.close()

def insert_values(database:str, table_name:str, header:list, data:list):
  parameters = ""
  for item in header:
    parameters += "%s, " if item != header[-1] else "%s"

  stmt = f"INSERT INTO {table_name} ({get_string_insert_cols(header)}) VALUES ({parameters})"

  connection = get_connection(database)
  cursor = connection.cursor()

  cursor.executemany(stmt, data)
  connection.commit()

  cursor.close()
  connection.close()