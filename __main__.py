import sys
from csv_to_mysql import csv_insert_mysql

title = "Conversor de archivos CSV"
print("")
print("="*len(title))
print(title)
print("="*len(title))

try:
  path = str(sys.argv[1])
  print(f"Estás intentando insertar el fichero {path}")
  print()
except:
  path = input("Ingresa la ruta del fichero csv: ")

database= input("Ingresa el nombre de la database MySQL: ")
table = input("Ingresa el nombre de la tabla: ")
print("")

csv_insert_mysql(path, table, database)