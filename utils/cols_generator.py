# FIXME: DETECT INT TYPE

def get_mysql_cols(header_types, primary_key = 'id'):
  mysql_cols = []
  has_primary_key = False
  for name, data_type in header_types.items():
    if data_type == 'int':
      col = f"`{name}` BIGINT {'PRIMARY KEY' if name == primary_key else 'NULL'}"
    elif data_type == 'float':
      col = f"`{name}` DOUBLE NULL"
    elif data_type == 'boolean':
      col = f"`{name}` BOOLEAN DEFAULT False"
    elif data_type == 'date':
      col = f"`{name}` DATE NULL"
    elif data_type == 'datetime':
      col = f"`{name}` DATETIME NULL"
    else:
      col = f"`{name}` VARCHAR(255) {'NULL PRIMARY KEY' if name == primary_key else 'NULL'}"
    if name == primary_key:
      has_primary_key = name == primary_key
    mysql_cols.append(col)
  if not has_primary_key:
    mysql_cols.insert(0, f"`{primary_key}` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY")
  return mysql_cols

def get_string_cols(cols:list):
  string_col = ""
  for col in cols:
    if col != cols[-1]:
      string_col += f"{col}, "
    else:
      string_col += col
  return string_col

def get_string_insert_cols(header:list):
  string_col = ""
  for name in header:
    if name != header[-1]:
      string_col += f"{name},"
    else:
      string_col += name
  return string_col