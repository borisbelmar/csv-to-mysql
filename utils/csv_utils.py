import csv
import re

def normalize(s):
  replacements = (
    ("á", "a"),
    ("é", "e"),
    ("í", "i"),
    ("ó", "o"),
    ("ú", "u"),
    ("ñ", "n"),
    (" ", "_")
  )
  for a, b in replacements:
    s = s.replace(a, b).replace(a.upper(), b.upper())
  return s

def get_data_header(header_row):
  header = []
  for item in header_row:
    header.append(re.sub("[^0-9a-zA-Z-_]+", "", normalize(item.strip().lower())))
  return header

def get_csv_data(path):
  csv_list = []
  csv_dict = []
  try:
    with open(path, encoding='utf-8-sig') as csv_file:
      csv_reader = csv.reader(csv_file, dialect='excel', delimiter=',')
      for row in csv_reader:
        csv_list.append(row)
    header = get_data_header(csv_list[0])
    for row in csv_list:
      if csv_list.index(row) != 0:
        new_dict = {}
        item_count = 0
        for item in row:
          if item_count < len(header):
            new_dict[header[item_count]] = item
            item_count += 1
        csv_dict.append(new_dict)
  except:
    print("El archivo que intentas insertar no existe o es inválido")
    exit()

  return {"data": csv_dict, "header": header, "count": len(csv_dict)}
  