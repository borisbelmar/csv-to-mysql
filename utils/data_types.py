from pytz import timezone
import calendar
from datetime import datetime
from dateutil.parser import parse as date_parser

def get_type(x:str):
  try:
    # Check int type
    int(x)
    return "int"
  except:
    try:
      # Check float type from number or percent
      float_string = x.replace("%", "")
      float(float_string)
      return "float"
    except:
      try:
        # Check date type
        timeformat = "%Y-%m-%d"
        datetime.strptime(x, timeformat)
        return "date"
      except:
        try:
          # TODO: Move this to another function
          date_parser(x)
          return "datetime"
        except:
          if x.lower() == 'true' or x.lower() == 'false':
            return "boolean"
          else:
            return "string"

def get_header_with_types(header:list, test_data:dict):
  header_with_types = {}
  for item in header:
    if item in test_data:
      typed_value = get_type(test_data[item])
      header_with_types[item] = typed_value
  return header_with_types

def int_converter(x:str):
  try:
    num = int(x)
    return num
  except:
    return None

def float_converter(x:str):
  try:
    float_string = x.replace("%", "")
    num = float(float_string)
    return num
  except:
    return None

def date_converter(x:str):
  try:
    timeformat = "%Y-%m-%d"
    date = datetime.strptime(x, timeformat)
    return date
  except:
    return None

def datetime_converter(x:str, for_mysql):
  try:
    timestamp = date_parser(x).timestamp()
    utc_date = datetime.utcfromtimestamp(timestamp)
    return utc_date if not for_mysql else utc_date.strftime('%Y-%m-%d %H:%M:%S')
  except:
    return None
    
def boolean_converter(x:str):
  return x.lower() == 'true'

def type_converter(x:str, data_type:str, for_mysql: bool):
  if data_type == "int":
    return int_converter(x)
  elif data_type == "float":
    return float_converter(x)
  elif data_type == "boolean":
    return boolean_converter(x)
  elif data_type == "date" and not for_mysql:
    return date_converter(x)
  elif data_type == "datetime":
    return datetime_converter(x, for_mysql)
  else:
    return x

def get_data_converted(total_data:dict, typed_header:dict, for_mysql:bool) -> dict:
  converted_data = []
  for item in total_data:
    converted_item = {}
    for name, value in item.items():
      try:
        converted_item[name] = type_converter(value, typed_header[name], True)
      except:
        converted_item[name] = None
    converted_data.append(converted_item)
  return converted_data

def get_data_in_tuples(data:list) -> list:
  data_in_tuples = []
  for item in data:
    data_in_tuples.append(tuple(item.values()))
  return data_in_tuples